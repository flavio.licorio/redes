package = b'xx\x1f\x12\x12\x08\x1c\x52\n\xc8\x02_G\xd2\x05b\xadi\x00\x18\x00\x02\xd4\n\x04\xbe\00\x04\xef\x00\x0f\xdb\x7f\r\n'
package = list(map(ord,package))
for i,j in zip(package,range(len(package))):
    print(str(hex(i)[2:].zfill(2))+" - "+str(i)+" - "+str(j)) 

def to_long(bytes,order=1):
    m = len(bytes)
    bytes = bytes[::order]
    result = 0
    for b,i in zip(bytes,range(len(bytes))):
        result += b * 2 ** (8 * (m-i))
    return result 

print(len(package))
parsed = dict(
    imei=package[0:2],
    data_type=package[2:6],
    type=package[6],
    lon=package[7:9],
    lat=package[9:11],
    speed=package[11],
    direction=package[12],
    date=package[13:16],
    time=package[16:19]
)

print(parsed)

